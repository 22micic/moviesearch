$(document).ready( () => {
    $('#searchForm').on('submit', (e) => {
        let searchText = $('#searchText').val();
        getMovies(searchText);
        e.preventDefault()
    })
});

function getMovies(searchText) {
    const proxy = 'https://cors-anywhere.herokuapp.com/'; // only for chrome
    const apiKey = '&apikey=6ff3a596';

    axios.get(proxy+'http://www.omdbapi.com/?s='+searchText+apiKey)
        .then((response) => {
            console.log(response);
            let movies = response.data.Search;
            let output = '';
            $.each(movies, (index, movie) => {
                output += `
                <div class="col-md-3 divClass"
                    <div class="well text-center">
                        <img src="${movie.Poster}" alt="">
                        <h5>${movie.Title}</h5>
                        <a onclick="movieSelected('${movie.imdbID}')" class="btn btn-primary" href="#">Movie details</a>
                    </div>
                </div>                    
                `;
            });
            $('#movies').html(output)
        })
        .catch((err) => {
            console.log(err)
        })
}

function movieSelected(id) {
    sessionStorage.setItem('movieId', id);
    window.location = 'movie.html';
    return false
}

function getMovie() {
    let movieId = sessionStorage.getItem('movieId')

    const proxy = 'https://cors-anywhere.herokuapp.com/'; // only for chrome
    const apiKey = '&apikey=6ff3a596';

    axios.get(proxy+'http://www.omdbapi.com/?i='+movieId+apiKey)
        .then((response) => {
            console.log(response);
            let movie = response.data;

            let output = `
            <div class="row">
                <div class="col-md-4">
                    <img class="imageDetails" src="${movie.Poster}">
                </div>   
                <div class="detailsOne">
                    <h2>${movie.Title}</h2>
                    <ul class="list-group">
                    <li class="list-group-item"><strong>Genre:</strong> ${movie.Genre}</li>
                    <li class="list-group-item"><strong>Released:</strong> ${movie.Released}</li>
                    <li class="list-group-item"><strong>Rated:</strong> ${movie.Rated}</li>
                    <li class="list-group-item"><strong>IMDB Rating:</strong> ${movie.imdbRating}</li>
                    <li class="list-group-item"><strong>Director:</strong> ${movie.Director}</li>
                    </ul>       
                </div> 
                <div class="detailsTwo">
                    <h5 style="text-align: left">Writers and actors</h5>
                    <ul class="list-group">
                    <li class="list-group-item"><strong>Writers:</strong> ${movie.Writer}</li>
                    <li class="list-group-item"><strong>Actors:</strong> ${movie.Actors}</li>
                    </ul>       
                </div>             
            </div>
            <div class="row">
                <div class="well">
                    <h3>Plot</h3>
                    ${movie.Plot}
                    <hr>
                    <a href="http://www.imdb.com/title/${movie.imdbID}/" target="_blank" class="btn btn-primary">View IMDB</a>
                </div>   
            </div>
            `;
            $('#movie').html(output)
        })
        .catch((err) => {
            console.log(err)
        })
}